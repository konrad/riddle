<?php

// init
$zahlen = [];

for($i=1;$i<=1000;$i++)
{
    $zahlen[] = ['ursp' => $i, 'jetzt' => $i];
}

$it = 0;
// Alle Zahlen durchgehen solange wir mehr als eine Zahl im Array haben.
while (count($zahlen)>1)
{
    $it++;

    $in = 0;
    // Das ganze Array mit allen dingern durchgehen und alle ungraden rauswerfen und ne neue Zahl zuweisen
    foreach($zahlen as $index => $zahl)
    {
        // Wenn ungrade, raus damit
        if($zahl['jetzt'] % 2 != 0)
        {
            unset($zahlen[$index]);
        }
        else
        {
            $in++;
            $zahlen[$index] = ['ursp' => $zahl['ursp'], 'jetzt' => $in];
        }
    }
}

echo "Durchgänge: $it\n";

$erg = array_values($zahlen)[0];

echo 'Die ursprüngliche Zahl ist '.$erg['ursp'].', beim letzten Durchgang hatte sie die Nummer '.$erg['jetzt']." \n";
